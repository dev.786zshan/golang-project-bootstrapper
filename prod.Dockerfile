FROM golang:alpine as builder

WORKDIR /go/src/app
ADD ./app/ ./
RUN go get -d -v
RUN GOARCH=386 GOOS=linux go build -ldflags="-w -s" -o /go/bin/app

FROM scratch
COPY --from=builder /go/bin/app /go/bin/app
EXPOSE 8080
ENTRYPOINT ["/go/bin/app"]