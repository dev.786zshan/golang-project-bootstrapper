FROM golang:alpine as builder

RUN apk update && apk add git
RUN go get github.com/githubnemo/CompileDaemon

# ENV GOOS=darwin GOARCH=amd64
WORKDIR /go/src/app

COPY ./docker-entrypoint.sh /docker-entrypoint.sh
RUN chmod +x /docker-entrypoint.sh

ADD ./app/ ./
RUN go get && go build

EXPOSE 8080
ENTRYPOINT ["/docker-entrypoint.sh"]
CMD CompileDaemon -log-prefix=false -build="go build" -command="./app"


# FROM alpine
# COPY --from=builder /go/src/gitlab.com/zeeshans/generator/generator /generator
# EXPOSE 8080
# CMD /generator