# Golang project bootstrapper
A basic Golang project bootstrapper - utilizing a Docker first approach.  
Project contains relevant dockerfiles to setup dev environment and prod based app deployment.  
In this example, a minimal Golang Web API has been bootstrapped.  

# Pre-requisites
- Docker

# Docker
## DEV
### Build Image (dev)
```docker build -t app-dev:go-alpine -f dev.Dockerfile .```
### Run App - with watch capabilities (dev)
```
docker run --rm -it \
  -p 8080:8080 \
  -v $(PWD)/app/:/go/src/app/ \
  app-dev:go-alpine
```

## PROD
### Build Image (prod)
```docker build -t app:go-scratch -f prod.Dockerfile .```
### Run App (prod)
```
docker run --rm -it \
  -p 8080:8080 \
  app:go-scratch
```